﻿$(document).ready(function () {


    $('input').attr('autocomplete', 'off');

    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
    });

    if (window.location.pathname == "/Catalogue" && sessionStorage.getItem("dateOrder") == null && userAuthorized)
        $('#chooseDateOrder').modal('show');

    if (!userAuthorized) {
        sessionStorage.clear();
    }

    $('#chooseDateOrder').on('show.bs.modal', function (e) {
        if (sessionStorage.getItem("dateOrder") != null)
            $("#dateLivraison").val(sessionStorage.getItem("dateOrder"))
    })

});





function navigateTo(link, dateOrder) {
    if (dateOrder != null) {
        if (dateOrder == "dateOrder")
            window.location.href = link + ((sessionStorage.getItem(dateOrder) != null) ? (link.indexOf("?") >= 0 ? "&" : "?") + "dateOrder=" + sessionStorage.getItem(dateOrder) : "");
    }
}

checkQuantity = (composeId, operation) => {
    var quantite = $("#" + composeId).val() * 1;
    if (quantite < 0) {
        alert("Veuillez saisir des quantités supérieures à zéro. ");

        console.log($("#" + composeId));

        return ;
    }

    quantite = (operation == null) ? quantite : (operation == "add") ? quantite + 1 : (quantite == 0) ? 0 : quantite - 1;
    $("#" + composeId).val(quantite);
    $("#add_" + composeId).css("display", "block");
}

function addToCart(composeId) {
    idArray = composeId.split("_");

    var data = {
        quantite: $("#" + composeId).val(),
        selectedRepasId: idArray[0],
        selectedFamilleRepasId: idArray[1],
        selectedProductId: idArray[2]
    }

    callAjax("/cart/plus/" + data.selectedRepasId + "/" + data.selectedFamilleRepasId + "/" + data.selectedProductId + "/" + data.quantite + "/" + sessionStorage.getItem("dateOrder"), "get", data,"", "reload");
}

function callAjax(remoteUrl, method, urlData, dateOrder, config) {

    if (dateOrder.length>0)
        remoteUrl += ((sessionStorage.getItem(dateOrder) != null) ? (remoteUrl.indexOf("?") >= 0 ? "&" : "?") + "dateOrder=" + sessionStorage.getItem(dateOrder) : "");


    if (method.toLowerCase() == "get") {

        $.ajax({
            url: remoteUrl,
            method: "get"
        })
        .done(data => {
            console.log(data);
            if (data.indexOf("http") >= 0)
                window.location.replace(data);

            if (config != null) {
                if (config == "reload")
                    window.location.reload();
            }
        })
        .fail(error => console.log(error))

    }
    else {
        $.ajax({
            url: remoteUrl,
            method: "post",
            data: urlData
        })
            .done(data => console.log(data))
            .fail(error => console.log(error))

    }

}

saveDateOrder = () => {
    console.log(window.location.pathname);
    if ($("#dateLivraison").val().length == 0) {
        $('#dateRequired').css("display", "block");
        return false;
    }

    sessionStorage.setItem("dateOrder", $("#dateLivraison").val());
    $('#chooseDateOrder').modal('hide');

    if (window.location.pathname == "/Catalogue")
        navigateTo(window.location.pathname,"dateOrder");
}

