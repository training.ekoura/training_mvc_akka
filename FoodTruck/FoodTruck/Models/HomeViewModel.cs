﻿using FoodTruck.DATA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FoodTruck.Models
{
    public class HomeViewModel
    {
        public IList<Actualite> Actualites { get; set; }

        public IList<Produit> Produits { get; set; }

    }
}