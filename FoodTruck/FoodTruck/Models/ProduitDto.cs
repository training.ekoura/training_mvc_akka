﻿using FoodTruck.DATA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FoodTruck.Models
{
    public class ProduitDto : Produit
    {
        public int quantite { get; set; }
        public string composedId { get; set; }
        public Repas Repas { get; set; }
    }
}