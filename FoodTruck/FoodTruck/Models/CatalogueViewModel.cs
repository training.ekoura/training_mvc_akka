﻿using FoodTruck.DATA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FoodTruck.Models
{
    public class CatalogueViewModel
    {
        public string desiredProduct { get; set; }
        public IList<ProduitDto> Produits { get; set; }
        public IList<RepasDto> Repas { get; set; }
        public IList<FamilleRepasDto> FamilleRepas { get; set; }

        public int SelectedRepasId { get; set; }

        public int SelectedFamilleRepasId { get; set; }

        public CatalogueViewModel()
        {
            Produits = new List<ProduitDto>();
            Repas = new List<RepasDto>();
            FamilleRepas = new List<FamilleRepasDto>();
        }
    }
}