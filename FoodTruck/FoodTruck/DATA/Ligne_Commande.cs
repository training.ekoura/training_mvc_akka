//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré à partir d'un modèle.
//
//     Des modifications manuelles apportées à ce fichier peuvent conduire à un comportement inattendu de votre application.
//     Les modifications manuelles apportées à ce fichier sont remplacées si le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FoodTruck.DATA
{
    using System;
    using System.Collections.Generic;
    
    public partial class Ligne_Commande
    {
        public int id_Ligne_Commande { get; set; }
        public double prix_unitaire { get; set; }
        public Nullable<int> quantite { get; set; }
        public string Repas_libelle { get; set; }
        public Nullable<int> id_Commande { get; set; }
        public Nullable<int> id_Produit { get; set; }
    
        public virtual Commande Commande { get; set; }
        public virtual Produit Produit { get; set; }
    }
}
