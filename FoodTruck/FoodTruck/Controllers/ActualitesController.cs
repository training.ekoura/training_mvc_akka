﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using FoodTruck.DATA;

namespace FoodTruck.Controllers
{
    public class ActualitesController : ApiController
    {
        private foodtruckEntitiesContext db = new foodtruckEntitiesContext();

        // GET: api/Actualites
        public IQueryable<Actualite> GetActualite()
        {
            return db.Actualite;
        }

        // GET: api/Actualites/5
        [ResponseType(typeof(Actualite))]
        public IHttpActionResult GetActualite(int id)
        {
            Actualite actualite = db.Actualite.Find(id);
            if (actualite == null)
            {
                return NotFound();
            }

            return Ok(actualite);
        }

        // PUT: api/Actualites/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutActualite(int id, Actualite actualite)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != actualite.id)
            {
                return BadRequest();
            }

            db.Entry(actualite).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ActualiteExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Actualites
        [ResponseType(typeof(Actualite))]
        public IHttpActionResult PostActualite(Actualite actualite)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Actualite.Add(actualite);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (ActualiteExists(actualite.id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = actualite.id }, actualite);
        }

        // DELETE: api/Actualites/5
        [ResponseType(typeof(Actualite))]
        public IHttpActionResult DeleteActualite(int id)
        {
            Actualite actualite = db.Actualite.Find(id);
            if (actualite == null)
            {
                return NotFound();
            }

            db.Actualite.Remove(actualite);
            db.SaveChanges();

            return Ok(actualite);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ActualiteExists(int id)
        {
            return db.Actualite.Count(e => e.id == id) > 0;
        }
    }
}