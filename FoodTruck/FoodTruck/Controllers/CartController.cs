﻿using FoodTruck.DATA;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace FoodTruck.Controllers
{
    [Authorize]
    [RoutePrefix("cart")]
    public class CartController : ApiController
    {

        private foodtruckEntitiesContext _db = new foodtruckEntitiesContext();

        //[Route("dateOrder/{dateOrder:datetime:regex(\\d{4}-\\d{2}-\\d{2})}")]
        //[HttpGet]
        //public void SetDateOrder( DateTime dateOrder)
        //{
        //    Session["dateOrder"] = dateOrder;
        //}
            
        [Route("plus/{selectedRepasId:int}/{selectedFamilleId:int}/{selectedProductId:int}/{quantite:int}/{dateOrder:datetime:regex(\\d{4}-\\d{2}-\\d{2})}")]
        [HttpGet]
        public IHttpActionResult AddCart(int selectedRepasId, int selectedFamilleId, int selectedProductId, int quantite, DateTime dateOrder)
        {
            // Product 
            var selectedProduct = _db.Produit.Find(selectedProductId);

            if (selectedProduct.disponibilite.IndexOf(((int)dateOrder.DayOfWeek).ToString()) > 0)
            { 
                var userEmail = RequestContext.Principal.Identity.Name;

                var selectedUser = _db.Utilisateur.FirstOrDefault(x => x.email.ToLower() == userEmail.ToLower());
                //session.Add("toto","test");
                if (selectedUser != null) {

                    // Check if commande exist
                    var orderInProgress = GetCommande(selectedUser, dateOrder);

                    if (orderInProgress.Ligne_Commande.Count() == 0) {
                        orderInProgress.Ligne_Commande = new List<Ligne_Commande>();
                        orderInProgress.Ligne_Commande.Add(
                                new Ligne_Commande() {
                                    Commande = orderInProgress,
                                    id_Produit = selectedProductId,
                                    prix_unitaire = selectedProduct.prix_unitaire,
                                    quantite = quantite,
                                    Repas_libelle = _db.Repas.Find(selectedRepasId).libelle
                                }
                            );
                    }
                    else {
                        var selectedLineCommandProduct = orderInProgress.Ligne_Commande
                                                                            .FirstOrDefault(l => l.id_Produit == selectedProductId);

                        if (selectedLineCommandProduct == null)
                        {
                            orderInProgress.Ligne_Commande.Add(
                                new Ligne_Commande()
                                {
                                    Commande = orderInProgress,
                                    id_Produit = selectedProductId,
                                    prix_unitaire = selectedProduct.prix_unitaire,
                                    quantite = quantite,
                                    Repas_libelle = _db.Repas.Find(selectedRepasId).libelle
                                }
                            );
                        }
                        else {
                            if (quantite > 0) { 
                                selectedLineCommandProduct.quantite = quantite;
                                _db.Entry(selectedLineCommandProduct).State = System.Data.Entity.EntityState.Modified;
                            }
                        }
                    }
                    // CheckUp
                    if (quantite == 0) {
                        var ligneCommande = orderInProgress.Ligne_Commande.FirstOrDefault(l => l.id_Produit == selectedProductId);
                        orderInProgress.Ligne_Commande.Remove(ligneCommande);
                    }

                    if (orderInProgress.Ligne_Commande.Count() == 0)
                        _db.Commande.Remove(orderInProgress);
                }


                _db.SaveChanges();
            }

            return Ok("");
        }

        private Commande GetCommande (Utilisateur _user, DateTime dateOrder)
        {
            var orderInProgress = _db.Commande.FirstOrDefault(x => x.id_Utilisateur == _user.id_Utilisateur && x.id_StatutCommande == 1 && x.dateCommande == dateOrder);

            if (orderInProgress == null)
            {
                orderInProgress = new Commande
                {
                    dateCommande = dateOrder,
                    num_facture =  Guid.NewGuid().ToString(),
                    prix_total = 12,
                    StatutCommande = _db.StatutCommande.Find(1),
                    Utilisateur = _user
                };

                _db.Commande.Add(orderInProgress);
                _db.SaveChanges();
            }
            return orderInProgress;
        }

        [Route("Delete")]
        [HttpGet]
        public string Delete([FromUri] int idProduit, [FromUri] DateTime dateOrder, [FromUri] string backToUrl="")
        {
            // Product 
            var selectedProduct = _db.Produit.Find(idProduit);

            if (selectedProduct.disponibilite.IndexOf(((int)dateOrder.DayOfWeek).ToString()) > 0) { 
                var userEmail = RequestContext.Principal.Identity.Name;

                var selectedUser = _db.Utilisateur.FirstOrDefault(x => x.email.ToLower() == userEmail.ToLower());
                //session.Add("toto","test");
                if (selectedUser != null)
                {

                    // Check if commande exist
                    var orderInProgress = GetCommande(selectedUser, dateOrder);

                    var selectedLine = orderInProgress.Ligne_Commande.FirstOrDefault(l => l.id_Produit == idProduit);
                    orderInProgress.Ligne_Commande.Remove(selectedLine);

                    if (orderInProgress.Ligne_Commande.Count() == 0)
                        _db.Commande.Remove(orderInProgress);

                    _db.SaveChanges();
                }
            }
            return backToUrl;


        }
    }
}
