﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FoodTruck.DATA;

namespace FoodTruck.Controllers
{
    public class ProduitController : Controller
    {
        private foodtruckEntitiesContext db = new foodtruckEntitiesContext();

        // GET: Produit
        public ActionResult Index()
        {
            var produit = db.Produit.Include(p => p.FamilleRepas);
            return View(produit.ToList());
        }

        // GET: Produit/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Produit produit = db.Produit.Find(id);
            if (produit == null)
            {
                return HttpNotFound();
            }
            return View(produit);
        }

        // GET: Produit/Create
        public ActionResult Create()
        {
            ViewBag.id_FamilleRepas = new SelectList(db.FamilleRepas, "id_FamilleRepas", "imageurl");
            return View();
        }

        // POST: Produit/Create
        // Pour vous protéger des attaques par survalidation, activez les propriétés spécifiques auxquelles vous souhaitez vous lier. Pour 
        // plus de détails, consultez https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_Produit,description,disponibilite,image,isenable,libellecommercial,libelletechnique,prix_unitaire,stock_dispo,id_FamilleRepas")] Produit produit)
        {
            if (ModelState.IsValid)
            {
                db.Produit.Add(produit);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_FamilleRepas = new SelectList(db.FamilleRepas, "id_FamilleRepas", "imageurl", produit.id_FamilleRepas);
            return View(produit);
        }

        // GET: Produit/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Produit produit = db.Produit.Find(id);
            if (produit == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_FamilleRepas = new SelectList(db.FamilleRepas, "id_FamilleRepas", "imageurl", produit.id_FamilleRepas);
            return View(produit);
        }

        // POST: Produit/Edit/5
        // Pour vous protéger des attaques par survalidation, activez les propriétés spécifiques auxquelles vous souhaitez vous lier. Pour 
        // plus de détails, consultez https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_Produit,description,disponibilite,image,isenable,libellecommercial,libelletechnique,prix_unitaire,stock_dispo,id_FamilleRepas")] Produit produit)
        {
            if (ModelState.IsValid)
            {
                db.Entry(produit).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_FamilleRepas = new SelectList(db.FamilleRepas, "id_FamilleRepas", "imageurl", produit.id_FamilleRepas);
            return View(produit);
        }

        // GET: Produit/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Produit produit = db.Produit.Find(id);
            if (produit == null)
            {
                return HttpNotFound();
            }
            return View(produit);
        }

        // POST: Produit/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Produit produit = db.Produit.Find(id);
            db.Produit.Remove(produit);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
