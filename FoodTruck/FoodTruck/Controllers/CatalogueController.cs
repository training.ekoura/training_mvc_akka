﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Web;
using System.Web.Http.Results;
using System.Web.Mvc;
using System.Web.Routing;
using FoodTruck.DATA;
using FoodTruck.Models;
using Microsoft.AspNet.Identity;

namespace FoodTruck.Controllers
{
    public class CatalogueController : Controller
    {
        private foodtruckEntitiesContext db = new foodtruckEntitiesContext();
        private  DateTime _today = DateTime.Now;
        private static readonly Expression<Func<Produit, ProduitDto>> AsProduitDTO = r => new ProduitDto
        {
            
        };
        // GET: Catalogue
        public ActionResult Index(int? SelectedRepasId, int? SelectedFamilleRepasId, DateTime? dateOrder = null )
        {
            // set default value
            var selectedDateOrder = dateOrder ?? DateTime.Now;
            Session["dateOrder"] = selectedDateOrder;
            Session["dateId"] = (int)selectedDateOrder.DayOfWeek;

            var vm = new CatalogueViewModel();
            vm.SelectedRepasId = SelectedRepasId ?? db.Repas.OrderBy(r => r.id_Repas).First().id_Repas;
            vm.SelectedFamilleRepasId = SelectedFamilleRepasId ?? db.Repas.Find(vm.SelectedRepasId).FamilleRepas.OrderBy(f => f.id_FamilleRepas).First().id_FamilleRepas;

            vm.Repas = db.Repas.Select( r => new RepasDto {
                id_Repas = r.id_Repas,
                FamilleRepas = r.FamilleRepas,
                heure_limite = r.heure_limite,
                imageurl = r.imageurl,
                isenabled = r.isenabled,
                libelle = r.libelle,
                selected = (vm.SelectedRepasId == r.id_Repas) ? true : false
            }).ToList();

            vm.FamilleRepas = db.Repas.Find(vm.SelectedRepasId).FamilleRepas
                .Select(f => new FamilleRepasDto { 
                    id_FamilleRepas = f.id_FamilleRepas,
                    imageurl = f.imageurl,
                    isenabled = f.isenabled,
                    libelle = f.libelle,
                    Produit = f.Produit,
                    Repas = f.Repas,
                    selected = (vm.SelectedFamilleRepasId == f.id_FamilleRepas) ? true : false
                })
                .OrderBy(f => f.id_FamilleRepas)
                .ToList() ;

            vm.Produits = db.Produit.Where(x => x.FamilleRepas.id_FamilleRepas == vm.SelectedFamilleRepasId)
                .Select(p => new ProduitDto { 
                    id_Produit = p.id_Produit,
                    id_FamilleRepas = p.id_FamilleRepas,
                    description = p.description,
                    disponibilite = p.disponibilite,
                    FamilleRepas = p.FamilleRepas,
                    image = p.image,
                    isenable = p.isenable,
                    libellecommercial = p.libellecommercial,
                    libelletechnique = p.libelletechnique,
                    prix_unitaire = p.prix_unitaire,
                    stock_dispo = p.stock_dispo,
                    quantite = 0,
                    composedId = vm.SelectedRepasId.ToString() + "_" + vm.SelectedFamilleRepasId + "_" + p.id_Produit
                })
                .ToList();

            foreach (var produit in vm.Produits)
            {
                produit.quantite = RetrieveQuantity(produit.id_Produit, selectedDateOrder);
            }

            return View(vm);
        }

        public int RetrieveQuantity(int idProduit, DateTime dateOrder)
        {
            if (User.Identity.IsAuthenticated) {
                var orderInProgress = db.Commande.FirstOrDefault(x => x.id_Utilisateur == db.Utilisateur.FirstOrDefault(u => User.Identity.Name.ToLower()== u.email.ToLower()).id_Utilisateur && x.dateCommande == dateOrder);
                if (orderInProgress != null)
                {
                    if (orderInProgress.Ligne_Commande.Count() == 0)
                        return 0;
                    else {
                        var selectedLine = orderInProgress.Ligne_Commande.FirstOrDefault(l => l.id_Produit == idProduit);
                        if (selectedLine == null)
                            return 0;
                        else
                            return int.Parse(selectedLine.quantite.ToString());
                    }
                }
                else { 
                    return 0;
                }

            }else
                return 0;
        }

        public ActionResult Panier() {
            return View(db.Commande.Include(x => x.Ligne_Commande)
                    .FirstOrDefault(x => x.id_Utilisateur == db.Utilisateur.FirstOrDefault(u => User.Identity.Name.ToLower() == u.email.ToLower()).id_Utilisateur)            
                    );
        }

        public ActionResult ResetCart() { 
            var orderInProgress = db.Commande.FirstOrDefault(x => x.id_Utilisateur == db.Utilisateur.FirstOrDefault(u => User.Identity.Name.ToLower()== u.email.ToLower()).id_Utilisateur);

            orderInProgress.Ligne_Commande.Clear();

            db.Commande.Remove(orderInProgress);

            db.SaveChanges();

            return RedirectToAction("panier");
               
        }

        public ActionResult Detail(int id)
        {
            if (id <= 0)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            
            var selectedProduct = db.Produit.Find(id);

            if (selectedProduct == null)
                return HttpNotFound();

            return View(selectedProduct);
        }
    }
}