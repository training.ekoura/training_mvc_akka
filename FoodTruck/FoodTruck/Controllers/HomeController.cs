﻿using FoodTruck.DATA;
using FoodTruck.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Web;
using System.Web.Mvc;

namespace FoodTruck.Controllers
{
    public class HomeController : Controller
    {
        private foodtruckEntitiesContext db = new foodtruckEntitiesContext();
        public ActionResult Index()
        {

            var vm = new HomeViewModel();
            vm.Actualites = db.Actualite.ToList();

            vm.Produits = db.Produit.Include(p => p.Ligne_Commande)
                                    .OrderByDescending(p => p.Ligne_Commande.Count)
                                    .Take(3)
                                    .ToList();

            return View(vm);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}