﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FoodTruck.DATA;

namespace FoodTruck.Controllers
{
    public class RepasController : Controller
    {
        private foodtruckEntitiesContext db = new foodtruckEntitiesContext();

        // GET: Repas
        public ActionResult Index()
        {
            var repasList = db.Repas.ToList();
            return View(repasList);
        }

        // GET: Repas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Repas repas = db.Repas.Find(id);
            if (repas == null)
            {
                return HttpNotFound();
            }
            return View(repas);
        }

        // GET: Repas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Repas/Create
        // Pour vous protéger des attaques par survalidation, activez les propriétés spécifiques auxquelles vous souhaitez vous lier. Pour 
        // plus de détails, consultez https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_Repas,heure_limite,imageurl,isenabled,libelle")] Repas repas)
        {
            if (ModelState.IsValid)
            {
                db.Repas.Add(repas);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(repas);
        }

        // GET: Repas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Repas repas = db.Repas.Find(id);
            if (repas == null)
            {
                return HttpNotFound();
            }
            return View(repas);
        }

        // POST: Repas/Edit/5
        // Pour vous protéger des attaques par survalidation, activez les propriétés spécifiques auxquelles vous souhaitez vous lier. Pour 
        // plus de détails, consultez https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_Repas,heure_limite,imageurl,isenabled,libelle")] Repas repas)
        {
            if (ModelState.IsValid)
            {
                db.Entry(repas).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(repas);
        }

        // GET: Repas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Repas repas = db.Repas.Find(id);
            if (repas == null)
            {
                return HttpNotFound();
            }
            return View(repas);
        }

        // POST: Repas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Repas repas = db.Repas.Find(id);
            db.Repas.Remove(repas);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
