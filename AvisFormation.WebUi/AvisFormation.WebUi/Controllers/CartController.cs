﻿using AvisFormation.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AvisFormation.WebUi.Controllers
{
    [Authorize]
    public class CartController : Controller
    {
        private AvisEntities db = new AvisEntities();
        // GET: Cart
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Add(int id)
        {

            var selectedFormation = db.Formation.Find(id);

            if (selectedFormation == null)
                return HttpNotFound();

            if (!((List<FormationAvecAvisDto>)Session["formations"]).Exists(x => x.Formation.Id == selectedFormation.Id)) {

                var dto = new FormationAvecAvisDto();
                dto.Formation = selectedFormation;
                if (selectedFormation.Avis.Count == 0)
                    dto.Note = 0;
                else
                    dto.Note = Math.Round(selectedFormation.Avis.Average(a => a.Note), 2);

                ((List<FormationAvecAvisDto>)Session["formations"]).Add(dto);
            }
            else {
                var newSelectedFormation = ((List<FormationAvecAvisDto>)Session["formations"]).FirstOrDefault(x => x.Formation.Id == selectedFormation.Id);
                ((List<FormationAvecAvisDto>)Session["formations"]).Remove(newSelectedFormation);
            }

            return RedirectToAction("TouteslesFormations", "Formation");
        }

        public ActionResult Delete(int id)
        {
            
            var selectedFormation = db.Formation.Find(id);

            if (selectedFormation == null)
                return HttpNotFound();

            if (((List<FormationAvecAvisDto>)Session["formations"]).Exists(x => x.Formation.Id == selectedFormation.Id))
            {
                var newSelectedFormation = ((List<FormationAvecAvisDto>)Session["formations"]).FirstOrDefault(x => x.Formation.Id == selectedFormation.Id);
                ((List<FormationAvecAvisDto>)Session["formations"]).Remove(newSelectedFormation);
            }

            return RedirectToAction("Index");
        }

        public ActionResult ResetCart()
        {
            ((List<FormationAvecAvisDto>)Session["formations"]).Clear();

            return RedirectToAction("Index");
        }
        
    }
}