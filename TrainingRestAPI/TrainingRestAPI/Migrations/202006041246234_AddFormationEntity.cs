namespace TrainingRestAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFormationEntity : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Formations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nom = c.String(nullable: false, maxLength: 100),
                        Url = c.String(),
                        Description = c.String(),
                        NomSeo = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Formations");
        }
    }
}
