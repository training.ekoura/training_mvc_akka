namespace TrainingRestAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Teacher",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TeacherName = c.String(),
                        TeacherLastName = c.String(),
                        TeacherBirthDate = c.DateTime(nullable: false),
                        TeacherIsSalary = c.Boolean(nullable: false),
                        TeacherPassword = c.String(),
                        TeacherIsActivated = c.Boolean(nullable: false),
                        TeacherLastSessionDate = c.DateTime(nullable: false),
                        TeacherEmail = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Teacher");
        }
    }
}
