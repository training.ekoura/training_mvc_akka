﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TrainingRestAPI.Models;

namespace TrainingRestAPI.Entities
{
    public class SMContext : DbContext
    {
        public DbSet<Teacher> Teacher { get; set; }
        public DbSet<Formation> Formation { get; set; }
    }

}