﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TrainingRestAPI.Models
{
    [Table("Teacher")]
    public class Teacher
    {
        [Column("ID")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TeacherId { get; set; }
        public string TeacherName { get; set; }
        public string TeacherLastName { get; set; }
        public DateTime TeacherBirthDate { get; set; }
        public bool TeacherIsSalary { get; set; }
        public string TeacherPassword { get; set; }
        public bool TeacherIsActivated { get; set; }
        public DateTime TeacherLastSessionDate { get; set; }
        public string TeacherEmail { get; set; }
    }
}