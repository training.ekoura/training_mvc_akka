﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TrainingRestAPI.Models.DTO
{
    public class FormationDTO
    {
        public int Id { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Le champ {0} doit supérieur à {2}", MinimumLength = 5)]
        public string Nom { get; set; }
        public string Url { get; set; }
    }
}