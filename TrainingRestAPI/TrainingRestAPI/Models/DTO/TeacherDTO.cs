﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TrainingRestAPI.Models.DTO
{
    public class TeacherDTO
    {
        public int TeacherId { get; set; }
        [Required]
        public string TeacherName { get; set; }
        [Required]
        public string TeacherLastName { get; set; }
        public DateTime TeacherBirthDate { get; set; }
        public bool TeacherIsSalary { get; set; }
        public string TeacherEmail { get; set; }
    }
}