﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using TrainingRestAPI.Models;
using TrainingRestAPI.Models.DTO;

namespace TrainingRestAPI.DAO
{
    public class FormationsDAO
    {
        private DatasourceManager _dataSourceManager;
        private readonly string teacherPassword = "ttttttttt";

        public FormationsDAO()
        {
            this._dataSourceManager = new DatasourceManager();
        }
        public List<Formation> FindAll()
        {
            List<Formation> _Formation = new List<Formation>();

            var conn = this._dataSourceManager.GetConnection();

            SqlCommand command;
            SqlDataReader dataReader;
            String sql = "SELECT Id,Nom,Url,Description,NomSeo FROM Formations";
            // 
            command = new SqlCommand(sql, conn);
            //
            dataReader = command.ExecuteReader();

            while (dataReader.Read())
            {
                _Formation.Add(new Formation()
                {
                    Id = dataReader.GetInt32(0),
                    Nom = (dataReader.IsDBNull(1) ? "" : dataReader.GetString(1)),
                    Url = (dataReader.IsDBNull(2) ? "" : dataReader.GetString(2)),
                    Description = (dataReader.IsDBNull(3) ? "" : dataReader.GetString(3)),
                    NomSeo = (dataReader.IsDBNull(4) ? "" : dataReader.GetString(4))
                });
            }

            dataReader.Close();
            command.Dispose();
            conn.Close();

            return _Formation;
        }

        public FormationDTO FindById(int Id)
        {

            FormationDTO _Formation = null;
            var _conn = this._dataSourceManager.GetConnection();

            String _sql = "SELECT Id,Nom,Url FROM Formations where Id=@Id;";
            
            SqlCommand _sqlCommand = new SqlCommand(_sql, _conn);
            _sqlCommand.Parameters.AddWithValue("@Id", Id);

            SqlDataReader _dataReader = _sqlCommand.ExecuteReader();

            while (_dataReader.Read())
            {
                _Formation = new FormationDTO()
                {
                    Id = _dataReader.GetInt32(0),
                    Nom = (_dataReader.IsDBNull(1) ? "" : _dataReader.GetString(1)),
                    Url = (_dataReader.IsDBNull(2) ? "" : _dataReader.GetString(2))
                };
            }

            _dataReader.Close();
            _sqlCommand.Dispose();
            _conn.Close();

            return _Formation;
        }

        //public TeacherDTO Create(TeacherDTO _teacher)
        //{

        //    try
        //    {
        //        var _conn = this._dataSourceManager.GetConnection();
        //        String _sql = "INSERT INTO Teacher(TeacherName, TeacherLastName, TeacherIsSalary, TeacherBirthDate, TeacherPassword , TeacherIsActivated, TeacherEmail, TeacherLastSessionDate ) " +
        //                        "Values(@TeacherName, @TeacherLastName, @TeacherIsSalary, @TeacherBirthDate, '" + this.teacherPassword + "' , 1, @TeacherEmail, getdate()); SELECT @@IDENTITY";

        //        SqlCommand _sqlCommand = new SqlCommand(_sql, _conn);
        //        _sqlCommand.Parameters.AddWithValue("@TeacherName", _teacher.TeacherName);
        //        _sqlCommand.Parameters.AddWithValue("@TeacherLastName", _teacher.TeacherLastName);
        //        _sqlCommand.Parameters.AddWithValue("@TeacherIsSalary", _teacher.TeacherIsSalary);
        //        _sqlCommand.Parameters.AddWithValue("@TeacherBirthDate", _teacher.TeacherBirthDate);
        //        _sqlCommand.Parameters.AddWithValue("@TeacherEmail", _teacher.TeacherEmail);
        //        SqlDataAdapter _dataAdapter = new SqlDataAdapter();
        //        _dataAdapter.InsertCommand = _sqlCommand;
        //        //_dataAdapter.InsertCommand.ExecuteNonQuery();

        //        var t = _sqlCommand.ExecuteScalar().ToString();
        //        _teacher.TeacherId = int.Parse(t);

        //        _sqlCommand.Dispose();
        //        _conn.Close();
        //        return _teacher;
        //    }
        //    catch (Exception x)
        //    {
        //        return null;
        //    }
        //}

        //public TeacherDTO Update(TeacherDTO _teacher)
        //{

        //    try
        //    {
        //        var _conn = this._dataSourceManager.GetConnection();
        //        String _sql = "Update Teacher set TeacherName = @TeacherName, TeacherLastName = @TeacherLastName, TeacherBirthDate=@TeacherBirthDate, TeacherEmail= @TeacherEmail " +
        //                        "where ID=@Id;";
        //        SqlCommand _sqlCommand = new SqlCommand(_sql, _conn);
        //        _sqlCommand.Parameters.AddWithValue("@TeacherName", _teacher.TeacherName);
        //        _sqlCommand.Parameters.AddWithValue("@TeacherLastName", _teacher.TeacherLastName);
        //        _sqlCommand.Parameters.AddWithValue("@TeacherBirthDate", _teacher.TeacherBirthDate);
        //        _sqlCommand.Parameters.AddWithValue("@TeacherEmail", _teacher.TeacherEmail);
        //        _sqlCommand.Parameters.AddWithValue("@Id", _teacher.TeacherId);
        //        SqlDataAdapter _dataAdapter = new SqlDataAdapter();
        //        _dataAdapter.UpdateCommand = _sqlCommand;
        //        _dataAdapter.UpdateCommand.ExecuteNonQuery();

        //        //var t = _sqlCommand.ExecuteScalar().ToString();
        //        //_teacher.TeacherId = int.Parse(t);

        //        _sqlCommand.Dispose();
        //        _conn.Close();
        //        return _teacher;
        //    }
        //    catch (Exception x)
        //    {
        //        return null;
        //    }
        //}

        //public TeacherDTO Delete(int teacherId)
        //{
        //    TeacherDTO _teacher = null;
        //    try
        //    {
        //        var _conn = this._dataSourceManager.GetConnection();
        //        String _sqlSelect = "select ID, TeacherName, TeacherLastName, TeacherIsSalary, TeacherBirthDate, TeacherPassword , TeacherIsActivated, TeacherLastSessionDate, TeacherEmail from Teacher where ID=@Id;";

        //        String _sql = "Delete from Teacher " +
        //                        "where ID=@Id;";
        //        SqlCommand _sqlCommand = new SqlCommand(_sqlSelect, _conn);
        //        _sqlCommand.Parameters.AddWithValue("@Id", teacherId);
        //        SqlDataReader _dataReader = _sqlCommand.ExecuteReader();

        //        if (_dataReader.HasRows)
        //        {

        //            while (_dataReader.Read())
        //            {
        //                _teacher = new TeacherDTO();
        //                _teacher.TeacherId = _dataReader.GetInt32(0);
        //                _teacher.TeacherName = (_dataReader.IsDBNull(1) ? "" : _dataReader.GetString(1));
        //                _teacher.TeacherLastName = (_dataReader.IsDBNull(2) ? "" : _dataReader.GetString(2));
        //                _teacher.TeacherIsSalary = (_dataReader.IsDBNull(3) ? false : _dataReader.GetBoolean(3));
        //                _teacher.TeacherBirthDate = (_dataReader.IsDBNull(4) ? new DateTime() : _dataReader.GetDateTime(4));
        //                _teacher.TeacherEmail = (_dataReader.IsDBNull(8) ? "" : _dataReader.GetString(8));
        //            }

        //            _dataReader.Close();


        //            SqlCommand _sqlCommandDelete = new SqlCommand(_sql, _conn);
        //            _sqlCommandDelete.Parameters.AddWithValue("@Id", teacherId);
        //            SqlDataAdapter _dataAdapter = new SqlDataAdapter();
        //            _dataAdapter.DeleteCommand = _sqlCommandDelete;
        //            _dataAdapter.DeleteCommand.ExecuteNonQuery();
        //        }
        //        //var t = _sqlCommand.ExecuteScalar().ToString();
        //        //_teacher.TeacherId = int.Parse(t);

        //        _sqlCommand.Dispose();
        //        _conn.Close();

        //        return _teacher;
        //    }
        //    catch (Exception x)
        //    {
        //        var error = x;
        //        return _teacher;
        //    }
        //}
    }
}