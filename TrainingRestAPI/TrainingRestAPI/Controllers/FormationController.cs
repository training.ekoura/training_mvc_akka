﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using TrainingRestAPI.Entities;
using TrainingRestAPI.Models;

namespace TrainingRestAPI.Controllers
{
    public class FormationController : ApiController
    {
        private SMContext db = new SMContext();

        // GET: api/Formation
        public IQueryable<Formation> GetFormation()
        {
            return db.Formation;
        }

        // GET: api/Formation/5
        [ResponseType(typeof(Formation))]
        public IHttpActionResult GetFormation(int id)
        {
            Formation formation = db.Formation.Find(id);
            if (formation == null)
            {
                return NotFound();
            }

            return Ok(formation);
        }

        // PUT: api/Formation/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutFormation(int id, Formation formation)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != formation.Id)
            {
                return BadRequest();
            }

            db.Entry(formation).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FormationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Formation
        [ResponseType(typeof(Formation))]
        public IHttpActionResult PostFormation(Formation formation)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Formation.Add(formation);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = formation.Id }, formation);
        }

        // DELETE: api/Formation/5
        [ResponseType(typeof(Formation))]
        public IHttpActionResult DeleteFormation(int id)
        {
            Formation formation = db.Formation.Find(id);
            if (formation == null)
            {
                return NotFound();
            }

            db.Formation.Remove(formation);
            db.SaveChanges();

            return Ok(formation);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FormationExists(int id)
        {
            return db.Formation.Count(e => e.Id == id) > 0;
        }
    }
}