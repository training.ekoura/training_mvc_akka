﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using TrainingRestAPI.DAO;
using TrainingRestAPI.Entities;
using TrainingRestAPI.Models;
using TrainingRestAPI.Models.DTO;

namespace TrainingRestAPI.Controllers
{
    public class HomeController : ApiController
    {
        // GET: api/Home

        private static readonly Expression<Func<Teacher, TeacherDTO>> AsTeacherDto =
          x => new TeacherDTO
          {
              TeacherId = x.TeacherId,
              TeacherEmail = x.TeacherEmail,
              TeacherBirthDate = x.TeacherBirthDate,
              TeacherName = x.TeacherName,
              TeacherLastName = x.TeacherLastName
          };
        public IHttpActionResult Get()
        {
            try
            {
                TeacherDAO teacherDAO = new TeacherDAO();

                SMContext db = new SMContext();
                var t = db.Teacher.ToList();
                // Test entity 
               


                    return Ok(teacherDAO
                                .FindAll()
                                .AsQueryable()
                                .Select(AsTeacherDto));
            }
            catch (Exception x)
            {
                var t = x;
                return InternalServerError();
            }
        }

        // GET: api/Home/5
        public IHttpActionResult Get(int id)
        {
            try
            {
                TeacherDAO teacherDAO = new TeacherDAO();
                var _teacher = teacherDAO.FindById(id);

                if (_teacher != null)
                    return Ok(
                                new TeacherDTO
                                {
                                    TeacherId = _teacher.TeacherId,
                                    TeacherEmail = _teacher.TeacherEmail,
                                    TeacherBirthDate = _teacher.TeacherBirthDate,
                                    TeacherName = _teacher.TeacherName,
                                    TeacherLastName = _teacher.TeacherLastName
                                }
                            );
                else
                    return NotFound();
            }
            catch (Exception e) {
                return InternalServerError();
            }

        }

        // POST: api/Home
        [HttpPost]
        public IHttpActionResult Post([FromBody]TeacherDTO _teacher)
        {
            try
            {
                TeacherDAO teacherDAO = new TeacherDAO();

                if (_teacher == null)
                    return BadRequest();

                var _newTeacher = teacherDAO.Create(_teacher);

                if (_newTeacher != null) {
                    return Created(Request.RequestUri + "/" + _newTeacher.TeacherId, _newTeacher);
                    //return Ok(_newTeacher);
                }
                else
                    return InternalServerError();
            }
            catch (Exception e)
            {
                return InternalServerError();
            }
        }

        // PUT: api/Home/5
        public void Put(int id, [FromBody]Teacher _teacher)
        {
        }

        // DELETE: api/Home/5
        public void Delete(int id)
        {
        }
    }
}
