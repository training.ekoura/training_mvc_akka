﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TrainingRestAPI.DAO;
using TrainingRestAPI.Models;
using TrainingRestAPI.Models.DTO;

namespace TrainingRestAPI.Controllers
{
    public class ADOFormationController : ApiController
    {
        // GET: api/ADOFormation
        private FormationsDAO db = new FormationsDAO();
        private static readonly Expression<Func<Formation, FormationDTO>> AsFormationDTO = z => new FormationDTO
        {
            Id = z.Id,
            Nom = z.Nom,
            Url = z.Url
        };


        public IEnumerable<FormationDTO> Get()
        {
            return db.FindAll()
                        .AsQueryable()
                        .Select(AsFormationDTO)
                        .ToList();
        }

        // GET: api/ADOFormation/5
        public IHttpActionResult Get(int id)
        {
            var selectedFormation = db.FindById(id);

            if (selectedFormation == null)
                return NotFound();

            return Ok(selectedFormation);
        }

        // POST: api/ADOFormation
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/ADOFormation/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/ADOFormation/5
        public void Delete(int id)
        {
        }
    }
}
