﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TrainingRestAPI.DAO;
using TrainingRestAPI.Models;
using TrainingRestAPI.Models.DTO;

namespace TrainingRestAPI.Controllers
{
    [RoutePrefix("prof")]
    public class ADOTeacherController : ApiController
    {

        private static readonly Expression<Func<Teacher, TeacherDTO>> AsTeacherDTO = z => new TeacherDTO
        {
            TeacherId = z.TeacherId,
            TeacherEmail = z.TeacherEmail,
            TeacherBirthDate = z.TeacherBirthDate,
            TeacherLastName = z.TeacherLastName,
            TeacherName = z.TeacherName
        };

        // GET: prof
        [Route("")]
        public IHttpActionResult Get()
        {
            try
            {
                return Ok(new TeacherDAO().FindAll()
                                        .AsQueryable()
                                        .Select(AsTeacherDTO));
            }
            catch (Exception x)
            {
                return InternalServerError();
            }
        }

        // GET: prof/5
        [Route("{id:int}")]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var lteacher = new TeacherDAO().FindById(id);

                if (lteacher != null)
                    return Ok(
                            new TeacherDTO
                            {
                                TeacherId = lteacher.TeacherId,
                                TeacherEmail = lteacher.TeacherEmail,
                                TeacherBirthDate = lteacher.TeacherBirthDate,
                                TeacherName = lteacher.TeacherName,
                                TeacherLastName = lteacher.TeacherLastName
                            }
                        );
                else
                    return NotFound();

            }
            catch (Exception x)
            {
                return InternalServerError();
            }
        }

        // POST: prof
        //Content-Type: application/json
        [Route("")]
        [HttpPost]
        public IHttpActionResult Post([FromBody]TeacherDTO teacher)
        {
            try
            {
                if (teacher == null)
                    return BadRequest("Contenu inexistant ....");

                if(!this.ModelState.IsValid)
                    return BadRequest(ModelState);


                var newTeacher = new TeacherDAO().Create(teacher);

                if (newTeacher != null)
                    return Created(Request.RequestUri + "/" + newTeacher.TeacherId, newTeacher);
                else
                    return InternalServerError();
            }
            catch (Exception x)
            {
                return InternalServerError();
            }
        }

        [Route("{id:int}")]
        [HttpPut]
        // PUT: prof/5
        public IHttpActionResult Put(int id, [FromBody]TeacherDTO teacher)
        {
            try
            {
                if (teacher == null)
                    return BadRequest();

                 return  Ok(new TeacherDAO().Update(teacher));

            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        // DELETE: prof/5
        [Route("{id:int}")]
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                var teacherDAO = new TeacherDAO();
                
                var deleteTeacher = teacherDAO.Delete(id);

                if (deleteTeacher != null)
                    return BadRequest();
                else
                    return Ok(deleteTeacher);

            }
            catch (Exception x)
            {
                return InternalServerError();
            }
        }
    }
}
