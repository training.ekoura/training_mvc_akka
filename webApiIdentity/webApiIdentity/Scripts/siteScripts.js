﻿//var token = "Y6kZozYIdrxurE0bAmq_MOiSaSTkOwXEimQJDXsrc07RsIZHBBM7C6bmRMG22FLOr2OCpxW14gPhV9KRzvngLR_Bp0rH-XiujjU1Sk_Yio4oN6gRMH5oLkcW8I15xNZyEClL81wTXwb0tMIBIIXi_VT5TH-5q0f4HgoNgm5NIGtPQvAfTYIDgSOLMLbP6VgbNdaWnRm-LmE268hLkkOA59iB904TJiBfM8Ki8pQRYgGSVYk0puj_PtcUQxLHlQ8rkAnrqmA-pxOF_aa23fD5zJzpXVK5feVJ4FHGDWtiqgkIxxLCwcwafmPg1CZ0P5VXN90-waVxDY9NLSZs6510IJKB9wpzmHCmS8siwLx9fkaJH6hMad-OlY6NpigTqsHu614k57Q7V-Vd7t4ql0v_y9qNTAkJF3L5zFwN_lFsd5l1wWpw3rSS7e1pBD_CYg-N81wC-HPfSMHSC1bCTf3COPcyhEjr_gS2CJSYSBZ4nlA";


function getAuthorizeResources() {
    var token = sessionStorage.getItem("tokenKey"); 
    var headers = {};
    headers.Authorization = 'Bearer ' + token;

    console.log(">>>>>> Enter getAuthorizeResources <<<<<<<  ");
    console.log(headers);

    $.ajax({
        type: 'GET',
        url: 'api/values',
        headers: headers
    }).done(function (data) {

        console.log("données reçues ==> ", data);

    }).fail(showError);
}


function RegisterUser() {
    console.log(">>>>>> Enter RegisterUser <<<<<<<  ");


    var data = {
        "email": $("#emailInput").val(), // document.getElementById("email").innerHtml
        "password": $("#passwordInput").val(),
        "confirmPassword": $("#confirmPasswordInput").val()
    }


    console.log("data => ", data); 

    $.ajax({
        type: 'post',
        url: 'api/Account/Register',
        data: data
    }).done(function (data) {

        console.log("données reçues ==> ", data);

    }).fail(showError);

    console.log(">>>>>> End RegisterUser <<<<<<<  ");


}

function Login() {

    console.log(">>>>>> Enter Login <<<<<<<  ");

    var data = "grant_type=password&username=" + $("#username").val() + "&password=" + $("#passwordLogin").val();


    console.log("data => ", data);

    $.ajax({
        type: 'post',
        url: 'Token',
        data: data
    }).done(function (data) {

        console.log("données reçues ==> ", data);
        sessionStorage.setItem("tokenKey", data.access_token);

    }).fail(showError);


    console.log(">>>>>> End Login <<<<<<<  ");

}

function showError(error) { console.log("error ==> ", error); }


$(document).ready(function () {

    var datas = [
        {
            "name": "Tiger Nixon",
            "position": "System Architect",
            "salary": "$3,120",
            "start_date": "2011/04/25",
            "office": "Edinburgh",
            "extn": "5421"
        },
        {
            "name": "Garrett Winters",
            "position": "Director",
            "salary": "$5,300",
            "start_date": "2011/07/25",
            "office": "Edinburgh",
            "extn": "8422"
        },
        {
            "name": "Garrett Winters",
            "position": "Director",
            "salary": "$5,300",
            "start_date": "2011/07/25",
            "office": "Edinburgh",
            "extn": "8422"
        },
        {
            "name": "Garrett Winters",
            "position": "Director",
            "salary": "$5,300",
            "start_date": "2011/07/25",
            "office": "Edinburgh",
            "extn": "8422"
        },
        {
            "name": "Garrett Winters",
            "position": "Director",
            "salary": "$5,300",
            "start_date": "2011/07/25",
            "office": "Edinburgh",
            "extn": "8422"
        },
        {
            "name": "Garrett Winters",
            "position": "Director",
            "salary": "$5,300",
            "start_date": "2011/07/25",
            "office": "Edinburgh",
            "extn": "8422"
        }
    ]

    $('#table_id').DataTable({
        data: datas,
        columns: [
            { data: 'name' },
            { data: 'salary' },
            { data: 'office' },
            { data: 'position' }
        ]
    });
});
